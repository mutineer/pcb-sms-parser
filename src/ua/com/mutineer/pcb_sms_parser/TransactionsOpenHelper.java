package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.BankTransaction;
import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TransactionsOpenHelper extends SQLiteOpenHelper {
	private static final int VERSION = 2;
	private static final String DATABASE_NAME = "Transactions";
	public static final String TRANSACTIONS_TABLE_NAME = "transactions";
	public static final String WIDGETS_TABLE_NAME = "widgets";

	public TransactionsOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TRANSACTIONS_TABLE_NAME + " (" + BankTransaction.Columns.ID             + " INTEGER PRIMARY KEY, "
																	+ BankTransaction.Columns.TIMESTAMP 	 + " INTEGER, "
																	+ BankTransaction.Columns.ACCOUNT   	 + " TEXT, "
																	+ BankTransaction.Columns.BALANCE        + " INTEGER, "
																	+ BankTransaction.Columns.LOCATION       + " TEXT, "
																	+ BankTransaction.Columns.VALUE          + " INTEGER, "
																	+ BankTransaction.Columns.EXPECTED_VALUE + " INTEGER, "
																	+ BankTransaction.Columns.PENDING        + " BOOLEAN );");
		
		db.execSQL("CREATE TABLE " + WIDGETS_TABLE_NAME + " (" + Utils.WidgetColumns.ID        + " INTEGER PRIMARY KEY, "
															   + Utils.WidgetColumns.ACCOUNT   + " TEXT, "
															   + Utils.WidgetColumns.WIDGET_ID + " INTEGER );");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion == 1 && newVersion == 2) {
			db.execSQL("CREATE TABLE " + WIDGETS_TABLE_NAME + " (" + Utils.WidgetColumns.ID        + " INTEGER PRIMARY KEY, "
					   										       + Utils.WidgetColumns.ACCOUNT   + " TEXT, "
					   										       + Utils.WidgetColumns.WIDGET_ID + " INTEGER );");
		}

	}

}
