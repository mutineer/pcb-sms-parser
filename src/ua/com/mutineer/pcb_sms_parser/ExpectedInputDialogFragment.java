package ua.com.mutineer.pcb_sms_parser;

import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.EditText;

public class ExpectedInputDialogFragment extends DialogFragment
										 implements DialogInterface.OnClickListener {
	private int hintValue;
	private int curValue;
	private long id;
	private EditText edit;
	
	private static final String HINT_KEY = "hintValue";
	private static final String CUR_KEY  = "curValue";
	private static final String ID_KEY   = "id";
	private static final String TEXT_KEY = "enteredText";
	
	public ExpectedInputDialogFragment(){
		super();
	}

	public ExpectedInputDialogFragment(long id, int value, int expected) {
		super();
		hintValue = (value < 0) ? -1 * value : value;
		curValue = (expected < 0) ? -1 * expected : expected;
		this.id = id;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(HINT_KEY, hintValue);
		outState.putInt(CUR_KEY,  curValue);
		outState.putLong(ID_KEY,  id);
		outState.putCharSequence(TEXT_KEY, edit.getText());
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.dlg_expectedValue_title);
		builder.setPositiveButton(android.R.string.ok, this);
		builder.setNegativeButton(android.R.string.cancel, null);
		
		if(savedInstanceState != null) {
			hintValue = savedInstanceState.getInt(HINT_KEY, hintValue);
			curValue  = savedInstanceState.getInt(CUR_KEY,  curValue);
			id        = savedInstanceState.getLong(ID_KEY,  id);
		}
		
		edit = new EditText(getActivity());
		edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		edit.setSingleLine();
		edit.setGravity(Gravity.RIGHT);
		edit.setInputType(InputType.TYPE_CLASS_NUMBER  | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		edit.setHint(String.format(Locale.US, "%.2f", (double)hintValue/100));
		if(savedInstanceState != null && savedInstanceState.containsKey(TEXT_KEY))
			edit.setText(savedInstanceState.getCharSequence(TEXT_KEY));
		else if(curValue != 0)
			edit.setText(String.format(Locale.US, "%.2f", (double)curValue/100));
		builder.setView(edit);
		return builder.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		int value = 0;
		try {
			value = -1 * Double.valueOf(Double.parseDouble(edit.getText().toString()) * 100).intValue();
		} catch (Exception e) {}
		if(value != curValue) {
			Intent intent = new Intent(getActivity(), WorkerService.class);
			intent.setAction(WorkerService.ACTION_UPDATE_TRANSACTION);
			intent.putExtra(WorkerService.EXTRA_TRANSACTION_ID, id);
			intent.putExtra(WorkerService.EXTRA_EXPECTED_VALUE, value);
			getActivity().startService(intent);
		}
	}

}
