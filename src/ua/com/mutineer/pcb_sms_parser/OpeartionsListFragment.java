package ua.com.mutineer.pcb_sms_parser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ua.com.mutineer.pcb_sms_parser.utils.BankTransaction;
import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class OpeartionsListFragment extends ListFragment
									implements LoaderCallbacks<Cursor>,
											   ActionBar.OnNavigationListener {

	private AnimatedCursorAdapter mAdapter;
	private ArrayAdapter<String> mSpinner;
	private String mFilter;
	
	private static final String FILTER_KEY = "accountFilter";
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(mFilter != null)
			outState.putString(FILTER_KEY, mFilter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Cursor cur = (Cursor) mAdapter.getItem(position);
		if(cur.getInt(mAdapter.colPending) != 0)
			new ExpectedInputDialogFragment(id, cur.getInt(mAdapter.colValue), 
												cur.getInt(mAdapter.colExpectedValue)).show(getFragmentManager(), "expected");
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if(savedInstanceState != null)
			mFilter = savedInstanceState.getString(FILTER_KEY, null);
		
		mSpinner = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item);
		SharedPreferences prefs = getActivity().getSharedPreferences(Utils.BALANCE_PREFERENCES, Context.MODE_PRIVATE);
		mSpinner.add("All");
		mSpinner.addAll(prefs.getAll().keySet());
		
		ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setListNavigationCallbacks(mSpinner, this);
		if(mFilter != null)
			actionBar.setSelectedNavigationItem( mSpinner.getPosition(mFilter) );
		
		setEmptyText(getResources().getText(R.string.no_transactions));
		setListShown(false);
		
		mAdapter = new AnimatedCursorAdapter(getActivity(), null, 0);
		setListAdapter(mAdapter);
		
		getLoaderManager().initLoader(0, null, this);
	}
	
	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int id,
			Bundle args) {
		Uri baseUri = Uri.withAppendedPath(Uri.parse("content://" + TransactionsProvider.AUTHORITY), TransactionsProvider.TRANSACTIONS_PATH);
		String[] projection = null;
		String selection = null; 
		String[] selectionArgs = null;
		String sortOrder = BankTransaction.Columns.TIMESTAMP + " DESC";
		if(mFilter != null) {
			selection = BankTransaction.Columns.ACCOUNT + " = ?";
			selectionArgs = new String[]{mFilter};
		}
		return new CursorLoader(getActivity(), baseUri, projection, selection, selectionArgs, sortOrder);
	}
	
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		String newFilter = (itemPosition == 0) ? null : mSpinner.getItem(itemPosition);
		
		if(mFilter == null && newFilter == null)
			return true;
		if(mFilter != null && mFilter.equals(newFilter))
			return true;
		
		mFilter = newFilter;
		getLoaderManager().restartLoader(0, null, this);
		return true;
	}
	
	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader,
			Cursor cursor) {
		mAdapter.swapCursor(cursor);
		if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
		
	}
	
	private static class AnimatedCursorAdapter extends CursorAdapter {
		private LayoutInflater inflater;
		private long firstAnimStart = 0;
		private SimpleDateFormat sdf;
		private int colAccount;
		private int colBalance;
		private int colValue;
		private int colExpectedValue;
		private int colDate;
		private int colLocation;
		private int colPending;
		
		private static class ViewHolder {
			TextView balanceText;
			TextView valueText;
			TextView expectedValueText;
			TextView dateText;
			TextView locationText;
			TextView accountText;
		}
		
		@Override
		public Cursor swapCursor(Cursor newCursor) {
			if(newCursor != null) {
				colAccount       = newCursor.getColumnIndex(BankTransaction.Columns.ACCOUNT);
				colBalance       = newCursor.getColumnIndex(BankTransaction.Columns.BALANCE);
				colValue         = newCursor.getColumnIndex(BankTransaction.Columns.VALUE);
				colExpectedValue = newCursor.getColumnIndex(BankTransaction.Columns.EXPECTED_VALUE);
				colDate          = newCursor.getColumnIndex(BankTransaction.Columns.TIMESTAMP);
				colLocation      = newCursor.getColumnIndex(BankTransaction.Columns.LOCATION);
				colPending       = newCursor.getColumnIndex(BankTransaction.Columns.PENDING);
			}
			return super.swapCursor(newCursor);
		}
		
		public AnimatedCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			sdf = new SimpleDateFormat("dd/MM/yyyy\nHH:mm:ss", Locale.US);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			int value, expected;
			value    = cursor.getInt(colValue);
			expected = cursor.getInt(colExpectedValue);
			
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.balanceText.setText(String.format("(%.2f)", (double)cursor.getInt(colBalance) / 100));
			holder.valueText.setText(String.format("%.2f", (double)value/100));
			holder.valueText.setTextColor( (value < 0) ? Color.argb(255, 204, 0, 0) : Color.argb(255, 102, 153, 0) );
			if(expected != value || cursor.getInt(colPending) != 0) {
				holder.expectedValueText.setText(String.format("(%.2f)", (double)expected/100));
				holder.expectedValueText.setVisibility(View.VISIBLE);
			} else {
				holder.expectedValueText.setVisibility(View.INVISIBLE);
			}
			holder.accountText.setText(cursor.getString(colAccount));
			holder.locationText.setText(cursor.getString(colLocation));
			holder.dateText.setText( sdf.format( new Date(cursor.getLong(colDate)) ));
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view = inflater.inflate(R.layout.transaction_list_item, null);
			ViewHolder holder = new ViewHolder();
			holder.balanceText       = (TextView) view.findViewById(R.id.listitem_text_balance);
			holder.valueText   		 = (TextView) view.findViewById(R.id.listitem_text_value);
			holder.expectedValueText = (TextView) view.findViewById(R.id.listitem_text_expected_value);
			holder.dateText          = (TextView) view.findViewById(R.id.listitem_text_date);
			holder.locationText      = (TextView) view.findViewById(R.id.listitem_text_location);
			holder.accountText       = (TextView) view.findViewById(R.id.listitem_text_account);
			view.setTag(holder);
			
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
				animateView(view, -1 * parent.getWidth(), cursor.getPosition());
			return view;
		}
		
		private void animateView(View view, int translation, int position) {
			long startDelay = 200;
			if(firstAnimStart == 0) {
				firstAnimStart = System.currentTimeMillis();
			} else {
				startDelay += position * (120 - position * 4) - (System.currentTimeMillis() - firstAnimStart);
			}
			
			if( startDelay >= 0) {
				view.setTranslationX(translation);
				view.animate().translationX(0).setDuration(400).setStartDelay(startDelay).
					 	 setListener(new PrivateAnimationListener(view)).
					 	 setInterpolator(new DecelerateInterpolator()).start();
			}
		}
		
		private static class PrivateAnimationListener implements AnimatorListener {
			private View v;
			private int  type;
			
			public PrivateAnimationListener(View view) {
				v = view;
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				v.setLayerType(type, null);
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationStart(Animator animation) {
				type = v.getLayerType();
				v.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			}	
		}	
	}
}
