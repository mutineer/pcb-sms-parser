package ua.com.mutineer.pcb_sms_parser;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivityOld extends PreferenceActivity
								 implements OnSharedPreferenceChangeListener {

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
		Preference phoneNumberPref = findPreference(SettingsFragment.PHONE_NUMBER_KEY);
		phoneNumberPref.setSummary(getPreferenceScreen().getSharedPreferences().getString(SettingsFragment.PHONE_NUMBER_KEY, ""));
	}

	@Override
	protected void onPause() {
		super.onPause();
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onSharedPreferenceChanged(SharedPreferences pref, String key) {
		if(key.equals(SettingsFragment.PHONE_NUMBER_KEY)) {
			Preference phoneNumberPref = findPreference(key);
			phoneNumberPref.setSummary(pref.getString(key, ""));
		}
	}
}
