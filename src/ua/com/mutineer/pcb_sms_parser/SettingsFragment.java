package ua.com.mutineer.pcb_sms_parser;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SettingsFragment extends PreferenceFragment
							  implements OnSharedPreferenceChangeListener {

	public final static String PHONE_NUMBER_KEY = "pref_bankPhoneNumber";
	public final static String PARSING_ENABLED_KEY = "pref_enableParsing";

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
		Preference phoneNumberPref = findPreference(PHONE_NUMBER_KEY);
		phoneNumberPref.setSummary(getPreferenceScreen().getSharedPreferences().getString(PHONE_NUMBER_KEY, ""));
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences pref, String key) {
		if(key.equals(PHONE_NUMBER_KEY)) {
			Preference phoneNumberPref = findPreference(key);
			phoneNumberPref.setSummary(pref.getString(key, ""));
		}
		
	}
	
}
