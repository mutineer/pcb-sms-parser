package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BalanceWidgetProvider extends AppWidgetProvider {

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		Log.d("WidgetProvider", "got on deleted");
		for(int i = 0; i < appWidgetIds.length; ++i) {
			Utils.removeWidgetEntry(context, appWidgetIds[i]);
		}
		
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onEnabled(Context context) {
		Log.d("WidgetProvider", "got on enabled");
		super.onEnabled(context);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		Log.d("WidgetProvider", "got on update");
		Intent intent = new Intent(context, WorkerService.class);
		intent.setAction(WorkerService.ACTION_UPDATE_WIDGETS);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		context.startService(intent);
		
		super.onUpdate(context, appWidgetManager, appWidgetIds);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
	    super.onReceive(context, intent);
	}

}
