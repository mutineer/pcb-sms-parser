package ua.com.mutineer.pcb_sms_parser.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ua.com.mutineer.pcb_sms_parser.InitActivity;
import ua.com.mutineer.pcb_sms_parser.MessageParsingService;
import ua.com.mutineer.pcb_sms_parser.R;
import ua.com.mutineer.pcb_sms_parser.SettingsFragment;
import ua.com.mutineer.pcb_sms_parser.TransactionsProvider;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Log;
import android.widget.RemoteViews;

public class Utils {
	public static final String BALANCE_PREFERENCES = "balances";
	
	public static final class WidgetColumns {
		public static final String ID        = BaseColumns._ID;
		public static final String ACCOUNT   = "account";       //TEXT
		public static final String WIDGET_ID = "widget_id";     //INTEGER
	}
	
	public static void updateWidgets(Context context, int[] ids) {
		if(context == null)
			throw new IllegalArgumentException("Context needed");
		if(ids == null || ids.length < 1)
			throw new IllegalArgumentException("Widget ids needed");
		
		ContentResolver cr = context.getContentResolver();
		Uri baseUri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
		baseUri = Uri.withAppendedPath(baseUri, TransactionsProvider.WIDGETS_PATH);
		
		SharedPreferences prefs = context.getSharedPreferences(BALANCE_PREFERENCES, Context.MODE_PRIVATE);
		AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
		
		for(int i = 0; i < ids.length; ++i) {
			//1) get account for widget
			Cursor c = cr.query(baseUri, 
							    new String[]{WidgetColumns.ACCOUNT}, 
							    WidgetColumns.WIDGET_ID + " = ?", 
							    new String[]{String.valueOf(ids[i])}, 
							    null);
			if( c != null && c.moveToFirst() ) {
				String account = c.getString(0);
				
				RemoteViews widgetViews = new RemoteViews(context.getPackageName(), R.layout.balance_widget_layout);
				
				Intent intent = new Intent(context, InitActivity.class);
				widgetViews.setOnClickPendingIntent(R.id.widget_balance_root, PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
				
				//2) get balance for account
				int balance = prefs.getInt(account, -1);
				if( balance != -1) {
					widgetViews.setTextViewText(R.id.widget_balance_text, String.format("%.2f", (double)balance / 100));
				} else {
					widgetViews.setTextViewText(R.id.widget_balance_text, context.getText(R.string.wgt_filler_text));
				}
				
				//3) update widget
				widgetManager.updateAppWidget(ids[i], widgetViews);
			} else {
				Log.w("Utils", "No account for widget " + String.valueOf(ids[i]));
			}
			if(c != null)
				c.close();
		}
	}
	
	public static void updateWidgets(Context context, String account) {
		if(account == null)
			throw new IllegalArgumentException("Account needed");
		if(context == null)
			throw new IllegalArgumentException("Context needed");
		
		int[] ids = null;
		Uri baseUri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
		baseUri = Uri.withAppendedPath(baseUri, TransactionsProvider.WIDGETS_PATH);
		baseUri = Uri.withAppendedPath(baseUri, account);
		
		ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(baseUri, new String[]{WidgetColumns.WIDGET_ID}, null, null, null);
		if(c != null && c.moveToFirst()) {
			ids = new int[c.getCount()];
			int i = 0;
			do {
				ids[i++] = c.getInt(0);
			} while(c.moveToNext());
			
			AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
			RemoteViews widgetViews = new RemoteViews(context.getPackageName(), R.layout.balance_widget_layout);
			SharedPreferences prefs = context.getSharedPreferences(BALANCE_PREFERENCES, Context.MODE_PRIVATE);
			
			Intent intent = new Intent(context, InitActivity.class);
			widgetViews.setOnClickPendingIntent(R.id.widget_balance_root, PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
			
			int balance = prefs.getInt(account, -1);
			if( balance != -1) {
				widgetViews.setTextViewText(R.id.widget_balance_text, String.format("%.2f", (double)balance / 100));
			} else {
				widgetViews.setTextViewText(R.id.widget_balance_text, context.getText(R.string.wgt_filler_text));
			}
			
			widgetManager.updateAppWidget(ids, widgetViews);
		} else {
			Log.d("Utils", "No widgets for account " + account);
		}
		if(c != null)
			c.close();
	}
	
	public static Uri saveTransactionToDB(Context context, BankTransaction transaction) {
		if(transaction == null)
			throw new IllegalArgumentException("Cannot save null transaction");
		if(context == null)
			throw new IllegalArgumentException("Context needed");
		
		ContentResolver cr = context.getContentResolver();
		Uri baseUri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
		Uri newUri = null;
		Cursor c = null;
		ContentValues values = null;
		
		c = cr.query(Uri.withAppendedPath(baseUri, TransactionsProvider.TRANSACTIONS_PATH),
				     new String[] {BankTransaction.Columns.ID, BankTransaction.Columns.BALANCE, BankTransaction.Columns.VALUE, BankTransaction.Columns.EXPECTED_VALUE}, 
				     BankTransaction.Columns.ACCOUNT + "=? AND " + BankTransaction.Columns.TIMESTAMP + "=?", 
				     new String[] {transaction.account, String.valueOf(transaction.timestamp.getTime())}, 
				     null);
		
		if(c != null && c.moveToFirst()) {
			if(!transaction.pending && transaction.value < 0) {
				int newBalance = c.getInt( 1 );
				newBalance += Math.abs( c.getInt(2) - transaction.value );
				
				values = new ContentValues();
				values.put(BankTransaction.Columns.VALUE,   transaction.value);
				values.put(BankTransaction.Columns.BALANCE, newBalance);
				values.put(BankTransaction.Columns.PENDING, false);
				if(c.getInt( 3 ) == 0)
					values.put(BankTransaction.Columns.EXPECTED_VALUE, transaction.value);
				
				newUri = Uri.withAppendedPath(baseUri, TransactionsProvider.TRANSACTIONS_PATH);
				newUri = ContentUris.withAppendedId(newUri, c.getLong( 0 ));
				
				cr.update(newUri, values, null, null);
				
				Log.d("Message saver", "updated " + newUri);
			} else {
				Log.d("Message saver", "message dropped");
			}
		} else {
			values = transaction.toContentValues();
			newUri = cr.insert( Uri.withAppendedPath(baseUri, TransactionsProvider.TRANSACTIONS_PATH), 
								values );
			Log.d("Message saver", "saved " + newUri);
		}
		if(c != null)
			c.close();
		return newUri;
	}
	
	public static void initiallyParseSMS(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		final String phoneNumber = prefs.getString(SettingsFragment.PHONE_NUMBER_KEY, MessageParsingService.DEFAULT_PHONE_NUMBER);
		
		Cursor cursor = context.getContentResolver().query( Uri.withAppendedPath(Uri.parse("content://sms"), "inbox"),
				   new String[] { "body" }, 
				   "address = ?", 
				   new String[] { phoneNumber }, 
				   "date DESC" );
		
		if( cursor != null && cursor.moveToFirst() ) {
			Map<String, Integer> balanceMap = new HashMap<String, Integer>();
			cursor.moveToPosition((cursor.getCount() <= 30) ? cursor.getCount() - 1 : 30);
			do {
				String body = cursor.getString(0);
				Log.d("Message saver", body);
				try {
					BankTransaction transaction = BankTransaction.parseString(body);
					balanceMap.put(transaction.account, transaction.balance);
					saveTransactionToDB(context, transaction);
				} catch (Exception e) {
					Log.e("Message saver", e.getMessage());
				}
			} while(cursor.moveToPrevious());
			
			if(!balanceMap.isEmpty()) {
				SharedPreferences.Editor editor = context.getSharedPreferences(BALANCE_PREFERENCES, Context.MODE_PRIVATE).edit();
				Iterator<Map.Entry<String, Integer>> it = balanceMap.entrySet().iterator();
				while( it.hasNext() ) {
					Map.Entry<String, Integer> pair = it.next();
					editor.putInt(pair.getKey(), pair.getValue());
					it.remove();
				}
				editor.commit();
			}
		}
		if(cursor != null)
			cursor.close();
	}
	
	public static void addNewWidgetEntry(Context context, int widgetId, String account) {
		if(account == null)
			throw new IllegalArgumentException("Account needed");
		if(context == null)
			throw new IllegalArgumentException("Context needed");
		
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues(2);
		Uri baseUri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
		
		values.put(WidgetColumns.ACCOUNT,   account);
		values.put(WidgetColumns.WIDGET_ID, widgetId);
		
		cr.insert( Uri.withAppendedPath(baseUri, TransactionsProvider.WIDGETS_PATH), values);
	}
	
	public static void removeWidgetEntry(Context context, int widgetId) {
		if(context == null)
			throw new IllegalArgumentException("Context needed");
		
		ContentResolver cr = context.getContentResolver();
		Uri baseUri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
		baseUri = Uri.withAppendedPath(baseUri, TransactionsProvider.WIDGETS_PATH);
		baseUri = ContentUris.withAppendedId(baseUri, widgetId);
		
		cr.delete(baseUri, null, null);
	}
}
