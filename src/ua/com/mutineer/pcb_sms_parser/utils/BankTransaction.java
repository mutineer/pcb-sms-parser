package ua.com.mutineer.pcb_sms_parser.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ContentValues;
import android.provider.BaseColumns;

public class BankTransaction {
	private static final String DEBIT              = "DEBIT";
	private static final String CREDIT             = "CREDIT";
	private static final String HOLD               = "HOLD";
	private static final String REGEXP             = "FUIB ACCOUNT CODE 19(\\d{8}) (?:\\*\\d{4} )?(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}) \\*(\\w+) (\\d+.\\d{2}) (\\w{3}) ([\\w\\W]*?) ?\\*BALANCE (\\d+.\\d{2}) \\5";
	/*  \1 Account
	 *  \2 Date
		\3 Type
		\4 Sum
		\5 Currency
		\6 Comment
		\7 BALANCE */
	
	public static final class Columns {
		public static final String ID             = BaseColumns._ID;
		public static final String TIMESTAMP      = "timestamp";      //INTEGER
		public static final String ACCOUNT        = "account";        //TEXT
		public static final String BALANCE        = "balance";        //INTEGER
		public static final String LOCATION       = "location";       //TEXT
		public static final String VALUE          = "value";          //INTEGER
		public static final String EXPECTED_VALUE = "expected_value"; //INTEGER
		public static final String PENDING        = "pending";        //BOOLEAN
	}
	
	public Date timestamp;
	public String account;
	public int balance;
	public int value;
	public int expected_value;
	public boolean pending;
	public String location;
	
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		
		values.put(Columns.ACCOUNT, 	   account);
		values.put(Columns.BALANCE, 	   balance);
		values.put(Columns.EXPECTED_VALUE, expected_value);
		values.put(Columns.LOCATION, 	   location);
		values.put(Columns.PENDING,        pending);
		values.put(Columns.TIMESTAMP,      timestamp.getTime());
		values.put(Columns.VALUE,          value);
		
		return values;
	}
	
	public static BankTransaction parseString(String string) 
			throws ParseException, NullPointerException {
		if(string == null)
			throw new NullPointerException("Message is empty!");
		
		Pattern MessagePattern = Pattern.compile(REGEXP);
		Matcher match = MessagePattern.matcher(string);
		
		if(!match.find())
			throw new ParseException("Message does not correnspond to pattern", 0);
		
		BankTransaction transaction = new BankTransaction();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		transaction.timestamp = sdf.parse( match.group(2) );
		transaction.location = match.group(6) == null ? "" : match.group(6);
		transaction.value = Double.valueOf( Double.parseDouble( match.group(4) ) * 100 ).intValue();
		transaction.balance = Double.valueOf( Double.parseDouble( match.group(7) ) * 100 ).intValue();
		transaction.account = match.group(1);
		transaction.expected_value = 0;
		
		if( match.group(3).equals(CREDIT) ) {
			transaction.pending  = false;
		} else if( match.group(3).equals(DEBIT) ) {
			transaction.value *= -1.0;
			transaction.pending  = false;
		} else if( match.group(3).equals(HOLD) ) {
			transaction.value *= -1.0;
			transaction.pending  = true;
		} else {
			throw new ParseException("Unknown operation type", 0);
		}
		
		if( !transaction.pending )
			transaction.expected_value = transaction.value;
		
		return transaction;
	}
}
