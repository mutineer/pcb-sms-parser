package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.BankTransaction;
import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.util.Log;

public class MessageParsingService extends IntentService {
	private static final String TAG = "MessageParsingService";
	public  static final String BALANCE_KEY = "BalanceKey";
	public  static final String DEFAULT_PHONE_NUMBER = "4682";

	public MessageParsingService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle bundle = intent.getExtras();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		final String phoneNumber = prefs.getString(SettingsFragment.PHONE_NUMBER_KEY, DEFAULT_PHONE_NUMBER);
		
		if(bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			SmsMessage msg = null;
			SharedPreferences.Editor balanceEditor = getSharedPreferences(Utils.BALANCE_PREFERENCES, MODE_PRIVATE).edit();
			
			for( int i = 0; i < pdus.length; ++i ) {
				msg = SmsMessage.createFromPdu( (byte[]) pdus[i] );
				Log.d(TAG, "Received message from " + msg.getDisplayOriginatingAddress());
				
				if( phoneNumber.compareTo( msg.getOriginatingAddress() ) == 0 ) {
					try {
						BankTransaction transaction = BankTransaction.parseString( msg.getDisplayMessageBody() );
						Utils.saveTransactionToDB(getApplicationContext(), transaction);
						
						balanceEditor.putInt(transaction.account, transaction.balance).commit();
						
						Utils.updateWidgets(getApplicationContext(), transaction.account);
					} catch (Exception e) {
						Log.e(TAG, e.getMessage());
					}
				}
			}
		}

	}

}
