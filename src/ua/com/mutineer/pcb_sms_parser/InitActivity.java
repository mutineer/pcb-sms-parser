package ua.com.mutineer.pcb_sms_parser;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class InitActivity extends ActionBarActivity {

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
	    setContentView(R.layout.init_layout);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings_button:
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
				startActivity(new Intent(this, SettingsActivity.class));
			else
				startActivity(new Intent(this, SettingsActivityOld.class));
			return true;
			
		case R.id.test_button: {
			startService( new Intent(WorkerService.ACTION_GET_INITIAL_MESSAGES, null, this, WorkerService.class));
			return true;
		}

		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
