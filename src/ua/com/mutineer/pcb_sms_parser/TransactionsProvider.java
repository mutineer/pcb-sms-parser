package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.BankTransaction;
import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class TransactionsProvider extends ContentProvider {
	private static final String TAG = "TransactionsProvider";
	public static final String AUTHORITY = "ua.com.mutineer.pcb_sms_parser.provider";
	public static final String TRANSACTIONS_PATH = "transactions";
	public static final String WIDGETS_PATH      = "widgets";
	
	private static final int TRANSACTIONS    = 1;
	private static final int TRANSACTION_ID  = 2;
	private static final int ACCOUNT_WIDGETS = 3;
	private static final int WIDGETS         = 4;
	private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	
	static {
		sUriMatcher.addURI(AUTHORITY, TRANSACTIONS_PATH,        TRANSACTIONS);
		sUriMatcher.addURI(AUTHORITY, TRANSACTIONS_PATH + "/#", TRANSACTION_ID);
		sUriMatcher.addURI(AUTHORITY, WIDGETS_PATH,             WIDGETS);
		sUriMatcher.addURI(AUTHORITY, WIDGETS_PATH + "/#",      ACCOUNT_WIDGETS);
	}
	
	private TransactionsOpenHelper mDBHelper;

	@Override
	public int delete(Uri uri, String whereClause, String[] whereArgs) {
		switch (sUriMatcher.match(uri)) {
		case ACCOUNT_WIDGETS:
			try {
				SQLiteDatabase db = mDBHelper.getWritableDatabase();
				int num = db.delete(TransactionsOpenHelper.WIDGETS_TABLE_NAME, 
						            Utils.WidgetColumns.WIDGET_ID + " = ?", 
						            new String[] {uri.getLastPathSegment()});

				getContext().getContentResolver().notifyChange(uri, null);
				return num;
			} catch (SQLiteException e) {
				Log.e(TAG, "Error opening db for write", e);
			}
			break;
			
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		case TRANSACTIONS:
			return "vnd.android.cursor.dir/ua.com.mutineer.transactions";
			
		case TRANSACTION_ID:
			return "vnd.android.cursor.item/ua.com.mutineer.transactions";
			
		case ACCOUNT_WIDGETS:
			return "vnd.android.cursor.dir/ua.com.mutineer.widgets";
			
		case WIDGETS:
			return "vnd.android.cursor.dir/ua.com.mutineer.widgets";
			
		default:
			return "";
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		switch( sUriMatcher.match(uri) ) {
		case TRANSACTIONS:
			if( values != null && values.containsKey(BankTransaction.Columns.ACCOUNT) &&
				values.containsKey(BankTransaction.Columns.BALANCE) &&
				values.containsKey(BankTransaction.Columns.PENDING) &&
				values.containsKey(BankTransaction.Columns.TIMESTAMP) &&
				values.containsKey(BankTransaction.Columns.VALUE)) {
					
					try {
						SQLiteDatabase db = mDBHelper.getWritableDatabase();
						
						long id = db.insertOrThrow(TransactionsOpenHelper.TRANSACTIONS_TABLE_NAME, null, values);
						
						getContext().getContentResolver().notifyChange(ContentUris.withAppendedId(uri, id), null);
						
						return ContentUris.withAppendedId(uri, id);
					} catch (SQLiteException e) {
						Log.e(TAG, "Error opening db for write", e);
					} catch (SQLException e) {
						Log.e(TAG, "Error creating new row", e);
						throw e;
					}
				}
			break;
			
		case WIDGETS:
			if( values != null && values.containsKey(Utils.WidgetColumns.ACCOUNT) &&
			    values.containsKey(Utils.WidgetColumns.WIDGET_ID)) {
				try {
					SQLiteDatabase db = mDBHelper.getWritableDatabase();
					
					db.insertOrThrow(TransactionsOpenHelper.WIDGETS_TABLE_NAME, null, values);
					
					getContext().getContentResolver().notifyChange(uri, null);
					
					return ContentUris.withAppendedId(uri, values.getAsInteger(Utils.WidgetColumns.WIDGET_ID));
				} catch (SQLiteException e) {
					Log.e(TAG, "Error opening db for write", e);
				} catch (SQLException e) {
					Log.e(TAG, "Error creating new row", e);
					throw e;
				}
			}
			break;
			
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);			
		}

		return null;
	}

	@Override
	public boolean onCreate() {
		mDBHelper = new TransactionsOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		
		switch( sUriMatcher.match(uri) ) {
		case TRANSACTIONS:
			qb.setTables(TransactionsOpenHelper.TRANSACTIONS_TABLE_NAME);
			break;
			
		case TRANSACTION_ID:
			qb.setTables(TransactionsOpenHelper.TRANSACTIONS_TABLE_NAME);
			qb.appendWhere(BaseColumns._ID + "=" + uri.getLastPathSegment());
			break;
			
		case WIDGETS:
			qb.setTables(TransactionsOpenHelper.WIDGETS_TABLE_NAME);
			break;
			
		case ACCOUNT_WIDGETS:
			qb.setTables(TransactionsOpenHelper.WIDGETS_TABLE_NAME);
			qb.appendWhere(Utils.WidgetColumns.ACCOUNT + "=" + uri.getLastPathSegment());
			break;
			
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		
		SQLiteDatabase db = mDBHelper.getReadableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		if( c != null )
			c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String whereClause, String[] whereArgs) {
		switch( sUriMatcher.match(uri) ) {
		case TRANSACTION_ID:
			try {
				SQLiteDatabase db = mDBHelper.getWritableDatabase();
				int num = db.update(TransactionsOpenHelper.TRANSACTIONS_TABLE_NAME, values, BaseColumns._ID + " = ?", new String[] {uri.getLastPathSegment()});

				getContext().getContentResolver().notifyChange(uri, null);
				return num;
			} catch (SQLiteException e) {
				Log.e(TAG, "Error opening db for write", e);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);			
		}
		return 0;
	}

}
