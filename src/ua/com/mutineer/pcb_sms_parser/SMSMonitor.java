package ua.com.mutineer.pcb_sms_parser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

public class SMSMonitor extends BroadcastReceiver {
	private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
	@SuppressWarnings("unused")
	private static final String TAG = "SMSMonitor";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if( PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsFragment.PARSING_ENABLED_KEY, false)) {
			if(ACTION.compareToIgnoreCase(intent.getAction()) == 0) {
				Intent startServ = new Intent(context, MessageParsingService.class);
				startServ.putExtras(intent.getExtras());
				context.startService(startServ);
			}
		}
	}

}
