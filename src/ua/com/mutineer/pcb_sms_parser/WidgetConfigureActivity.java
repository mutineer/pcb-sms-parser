package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class WidgetConfigureActivity extends ListActivity {
	private static final String TAG = "WidgetConfigureActivity";
	private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		String account = (String) getListAdapter().getItem(position);
		Log.d("TAG", account);
		
		finishForResult(account);
	}
	
	private void finishForResult(String account) {
		Intent intent = new Intent();
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
		setResult(RESULT_OK, intent);
		
		intent = new Intent(WorkerService.ACTION_ADD_WIDGET, null, this, WorkerService.class);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
		intent.putExtra(WorkerService.EXTRA_WIDGET_ACCOUNT, account);
		startService(intent);
		
		intent = new Intent(WorkerService.ACTION_UPDATE_WIDGETS, null, this, WorkerService.class);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[]{widgetId});
		startService(intent);
		
		finish();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setResult(RESULT_CANCELED);
	    
	    Bundle extras = getIntent().getExtras();
	    if(extras != null)
	    	widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
	    if(widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
	    	Log.e(TAG, "Invalid widget id!");
	    	finish();
	    }
	    	
	    SharedPreferences balances = getSharedPreferences(Utils.BALANCE_PREFERENCES, MODE_PRIVATE);
	    String[] accounts = balances.getAll().keySet().toArray(new String[]{});
	    
	    if(accounts.length == 1) {
	    	finishForResult(accounts[0]);
	    }
	    
	    if(accounts.length == 0) {
	    	setTitle(R.string.wgt_config_no_accounts);
	    } else {
	    	setTitle(R.string.wgt_config_title);
	    }
	    
	    setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, accounts));
	}

}
