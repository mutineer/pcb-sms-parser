package ua.com.mutineer.pcb_sms_parser;

import ua.com.mutineer.pcb_sms_parser.utils.BankTransaction;
import ua.com.mutineer.pcb_sms_parser.utils.Utils;
import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class WorkerService extends IntentService {
	public static final String ACTION_UPDATE_WIDGETS = "ua.com.mutineer.ACTION_UPDATE_WIDGETS";	
	public static final String ACTION_GET_INITIAL_MESSAGES = "ua.com.mutineer.ACTION_GET_INITIAL_MESSAGES";
	public static final String ACTION_ADD_WIDGET = "ua.com.mutineer.ACTION_ADD_WIDGET";
	public static final String EXTRA_WIDGET_ACCOUNT = "widgetAccount";
	
	public static final String ACTION_UPDATE_TRANSACTION = "ua.com.mutineer.ACTION_UPDATE_TRANSACTION";
	public static final String EXTRA_TRANSACTION_ID = "transactionId";
	public static final String EXTRA_EXPECTED_VALUE = "expectedValue";

	public WorkerService() {
		super("Worker service");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if( ACTION_UPDATE_WIDGETS.equals( intent.getAction() ) ) {
			Utils.updateWidgets(this, intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS));
			
		} else if( ACTION_GET_INITIAL_MESSAGES.equals( intent.getAction() ) ) {
			Utils.initiallyParseSMS(this);
			
		} else if( ACTION_ADD_WIDGET.equals( intent.getAction() )) {
			if(intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID) &&
			   intent.hasExtra(EXTRA_WIDGET_ACCOUNT)) {
				Utils.addNewWidgetEntry(this,  
										intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID), 
										intent.getStringExtra(EXTRA_WIDGET_ACCOUNT));
			} else {
				Log.e("Worker service", "Not enouth parameters to save new widget entry");
			}
		} else if( ACTION_UPDATE_TRANSACTION.equals( intent.getAction() )) {
			if(intent.hasExtra(EXTRA_TRANSACTION_ID)) {
				ContentValues values = new ContentValues();
				if(intent.hasExtra(EXTRA_EXPECTED_VALUE))
					values.put(BankTransaction.Columns.EXPECTED_VALUE, intent.getIntExtra(EXTRA_EXPECTED_VALUE, 0));
				
				if(values.size() != 0) {
					Uri uri = Uri.parse("content://" + TransactionsProvider.AUTHORITY);
					uri = Uri.withAppendedPath(uri, TransactionsProvider.TRANSACTIONS_PATH);
					uri = ContentUris.withAppendedId(uri, intent.getLongExtra(EXTRA_TRANSACTION_ID, 0));
					getContentResolver().update(uri, values, null, null);
				}
			} else {
				Log.e("Worker service", "Please provide transaction id");
			}
		}

	}

}
